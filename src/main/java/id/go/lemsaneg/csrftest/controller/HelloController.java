package id.go.lemsaneg.csrftest.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HelloController {

    @RequestMapping(value = { "/", "/welcome**" }, method = RequestMethod.GET)
    public ModelAndView welcomePage() {

        ModelAndView model = new ModelAndView();
        model.addObject("title", "Spring Security Hello World");
        model.addObject("message", "This is welcome page!");
        model.setViewName("hello");
        return model;

    }

    @RequestMapping(value = "/csrf/testcsrf.htm", method = RequestMethod.GET)
    public ModelAndView adminCsrf() {

        ModelAndView model = new ModelAndView();
        model.addObject("title", "Spring Security Hello World");
        model.addObject("message", "This is protected page!");
        model.setViewName("testcsrf");

        return model;

    }

    @RequestMapping(value = "/csrf/testcsrf.htm", method = RequestMethod.POST)
    public ModelAndView adminCsrfPost(HttpServletRequest request) {
        String text = ServletRequestUtils.getStringParameter(request, "data", "");

        ModelAndView model = new ModelAndView();
        model.addObject("title", "Spring Security POST CSRF");
        model.addObject("data", text);
        model.setViewName("testcsrfpost");

        return model;

    }

}
