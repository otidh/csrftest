<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="false"%>
<html>
<body>
	<h1>Title : ${title}</h1>	
	<h1>Message : ${message}</h1>
	<c:url var="linkUrl" value="/csrf/testcsrf.htm"/>
	Click <a href="${linkUrl}">here</a> to input form.	
</body>
</html>